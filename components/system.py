#!/usr/bin/env python

from subprocess import Popen, PIPE
import re 
from datetime import timedelta

from .logger import log


class SysInfo:
    """
    Provides Diagnostic Methods for the Machine running this Module.
    """
    
    def uptime(self):
        """
        Returns the current System Uptime in human-readable format.
        Decodes output of "uptime" command.
        """
        log.info('Extracting System Uptime')

        try:
            up = Popen('uptime', stdout=PIPE).stdout.readline()
            regex = re.compile('up\W+(.*?),\W+\d\W+user')
            up = regex.findall(up.decode('utf8'))[0]
        except:
            up = 'unknown'
            
        return up        
        
        
    def info(self):
        """
        Returns some System Infos about this Machine.
        """
        log.info('Extracting System Infos')
        
        uptime = Popen('uptime', shell='true',
                        stdout=PIPE).stdout.read().decode('utf-8')
        version = Popen('cat /proc/version', shell='true', 
                        stdout=PIPE).stdout.read().decode('utf-8')
        meminfo = Popen('cat /proc/meminfo', shell='true',
                        stdout=PIPE).stdout.read().decode('utf-8')
        modules = Popen('cat /proc/modules', shell='true', 
                        stdout=PIPE).stdout.read().decode('utf-8')
                            
        info = "UPTIME: %s\n\nVERSION: %s\n\nMEMORY: %s\n\nMODULES: %s"\
                % (uptime, version, meminfo, modules)
        return info
        
        
    def syslog(self):
        """
        Returns the Last 100 Lines from the info.log File
        """
        command = Popen('tail -n100 log/info.log', shell='true', stdout=PIPE)
        output = command.stdout.read().decode('utf-8')
        return output
        
    
    def __init__(self):
        log.info('Initialized System Informations Module')
    
    
    
# TEST INVOCATION
if __name__ == '__main__':
    print(SysInfo().uptime())