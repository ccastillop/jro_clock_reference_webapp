#!/bin/env python

import struct                       # For Binary Structs
from collections import namedtuple  # For Binary Structs

from .logger import log

# Checks if Serial Port is available, else switches to Debug Mode.
# This happens eg. when run on a non-Raspberry-Pi machine.
try:
    import serial
except:
    log.error("GPS Error: Couldn't find Module serial.\
               Switching to Debug Mode.")
    global _DEBUG_
    _DEBUG_ = 1
    
    

class MotorolaGPS:
    """
    Enables Communication with a Motorola M12 GPS Module.
    
    The proprietary Motorola Binary Protocol is used over a UART connection.
    Provides Methods to check current status (coordinates/time/RSSI)
    and manage Position Hold Mode
    """
    
    # Private Class Variables
    #########################
    serport = '/dev/ttyAMA0'
    uart = None
    
    # Messages to GPS Module (Constants)
    ####################################
    #( prefix, postfix and checksum added in self.msg() )
    
    # <ASCII POSITION MESSAGE>
    MSG_ASCII_POSITION_POLL = ['E', 'q', 0x00]
    MSG_ASCII_POSITION_1SEC = ['E', 'q', 0x01]
    
    # <POSITION/STATUS/DATA MESSAGE>
    MSG_POSITION_STATUS_DATA_POLL = ['H', 'a', 0x00]
    MSG_POSITION_STATUS_DATA_1SEC = ['H', 'a', 0x01]
    
    # <POSITION-HOLD POSITION> with garbage Input -> get current Position-Hold Position
    MSG_POSITION_HOLD_POSITION_STATUS = ['A', 's', 0x7f, 0xff, 0xff,
        0xff, 0x7f, 0xff, 0xff, 0xff, 0x7f, 0xff, 0xff, 0xff, 0xff]
        
    # <POSITION CONTROL>
    MSG_POSITION_HOLD_ENABLE   = ['G', 'd', 0x01]   # Enable Position Hold
    MSG_POSITION_HOLD_DISABLE  = ['G', 'd', 0x00]   # Disable Position Hold
    MSG_POSITION_HOLD_STATUS   = ['G', 'd', 0xff]   # Get current Status

    # Answer Messages from GPS Module (Constants)
    #############################################
    # (Answers are binary structures, use packages struct
    #  and collections.namedtuple to expand into objects)
    
    # <POSITION/STATUS/DATA MESSAGE>
    # @@HamdyyhmsffffaaaaoooohhhhmmmmaaaaoooohhhhmmmmVVvvhhddntimsidd[repeat imsidd for remaining 11 channels]                         ssrrccooooTTushmvvvvvvC<CR><LF>
    # 4s  BBH BBBI   i   i   i   i   i   i   i   i   H H H H BBBBBBH BBBBH BBBBH BBBBH BBBBH BBBBH BBBBH BBBBH BBBBH BBBBH BBBBH BBBBH H xxh I   h BBBB6s    B 2s
    STRUCT_FORMAT_STATUS = ">4sBBHBBBIiiiiiiiiHHHHBBBBBBHBBBBHBBBBHBBBBHBBBBHBBBBHBBBBHBBBBHBBBBHBBBBHBBBBHBBBBHHxxhIhBBBB6sB2s"
    STRUCT_NAMES_STATUS = 'msg_id month day year hours minutes seconds frac_seconds \
        lat_filt long_filt height_gps_filt height_msl_filt \
        lat_raw long_raw height_gps_raw height_msl_raw \
        speed_3d speed_2d heading_2d \
        dop sat_vis sat_track \
        ch01_svid ch01_mode ch01_signal_strength ch01_iode ch01_status \
        ch02_svid ch02_mode ch02_signal_strength ch02_iode ch02_status \
        ch03_svid ch03_mode ch03_signal_strength ch03_iode ch03_status \
        ch04_svid ch04_mode ch04_signal_strength ch04_iode ch04_status \
        ch05_svid ch05_mode ch05_signal_strength ch05_iode ch05_status \
        ch06_svid ch06_mode ch06_signal_strength ch06_iode ch06_status \
        ch07_svid ch07_mode ch07_signal_strength ch07_iode ch07_status \
        ch08_svid ch08_mode ch08_signal_strength ch08_iode ch08_status \
        ch09_svid ch09_mode ch09_signal_strength ch09_iode ch09_status \
        ch10_svid ch10_mode ch10_signal_strength ch10_iode ch10_status \
        ch11_svid ch11_mode ch11_signal_strength ch11_iode ch11_status \
        ch12_svid ch12_mode ch12_signal_strength ch12_iode ch12_status \
        receiver_status \
        clock_bias oscillator_offset temperature \
        utc_params \
        gmt_offset_s gmt_offset_h gmt_offset_m \
        id_tag checksum newline'
    # END MSG_POSITION_STATUS_DATA_POLL Answer

    # INFO_MESSAGE FORMAT
    
    info_msg = """
    GPS Status:
    ================
    Time: %s
    Satellites Visible: %d
    Satellites Tracked: %d
    DOP: %s m
    Lat: %s mas
    Lon: %s mas
    Height (GPS): %s cm
    Receiver Status: %#x
        Bit 15-13 (Fix Mode): %#x (%s)
        Bit 9 (Fast Acquisition): %s
        Bit 8 (Filter Reset): %s
        Bit 7 (Cold Start): %s
        Bit 6 (Differential Fix): %s
        Bit 5 (Position Lock): %s
        Bit 4 (Autosurvey Mode): %s
        Bit 3 (Insufficient Visible Satellites): %s
        Bit 2-1 (Antenna Sense): %#x (%s)
        Bit 0 (Code Location): %#x (%s)
    Temperature (degC): %s"""
    #
    
    
    #### HIGH-LEVEL METHODS ###
    
    def status(self):
        """
        Returns a tuple (icon, status) consisting of an icon 'alert'/'okay'
        and a Status Message String indicating the current Mode of Operation:
            - alert: No Fix, xx/yy Satellites tracked.
            - alert: 2D Fix, xx/yy Satellites tracked.
            - okay: 3D Fix, xx/yy Satellites tracked.
        """
        
        log.info("Creating Short Status Notice for GPS.")
        
        status = self.get_status()
        icon = 'alert'
        
        fix_mode = (status.receiver_status >> 13) & (0x07)
        if fix_mode == 7: 
            fix_mode_str = '3D Fix' 
            icon = 'okay'
        elif fix_mode == 6: fix_mode_str = '2D Fix'
        elif fix_mode == 5: fix_mode_str = 'Propagate'
        elif fix_mode == 4: fix_mode_str = 'Position Hold'
        elif fix_mode == 3: fix_mode_str = 'Acquiring Satellites' 
        elif fix_mode == 2: fix_mode_str = 'Bad Geometry'
        else: fix_mode_str = 'Error'
        
        msg = "%s, %s/%s Satellites tracked." % (fix_mode_str, status.sat_track, status.sat_vis)
        
        return (icon, msg)
        
        
    def info(self):
        """
        Returns GPS status report as string,
        using all data from POSITION/STATUS/DATA message.
        """
        log.info("Creating Full Status Report for GPS.")
        
        status = self.get_status()
        
        fix_mode = (status.receiver_status >> 13) & (0x07)
        if fix_mode == 7: fix_mode_str = '3D' 
        elif fix_mode == 6: fix_mode_str = '2D'
        elif fix_mode == 5: fix_mode_str = 'Propagate'
        elif fix_mode == 4: fix_mode_str = 'Position Hold'
        elif fix_mode == 3: fix_mode_str = 'Acq. Sat.' 
        elif fix_mode == 2: fix_mode_str = 'Bad Geom.'
        else: fix_mode_str = 'Error.'

        antenna_mode = (status.receiver_status >> 1) & (0x03)
        if antenna_mode == 0: antenna_mode_str = 'OK'
        elif antenna_mode == 1: antenna_mode_str = 'Overcurrent, Short' 
        elif antenna_mode == 2: antenna_mode_str = 'Undercurrent, Open' 
        elif antenna_mode == 3: antenna_mode_str = 'NV' 
        else: antenna_mode_str = 'Error.'
        
        msg = self.info_msg % (
            "%s/%s/%s %s:%s:%s.%s" % (status.year, status.month, status.day, status.hours, status.minutes, status.seconds, status.frac_seconds),
            status.sat_vis, status.sat_track,
            status.dop/10.0,
            status.lat_filt, status.long_filt, status.height_gps_filt,
            status.receiver_status,
            fix_mode, fix_mode_str,
            (status.receiver_status >> 9) & 0x01,
            (status.receiver_status >> 8) & 0x01,
            (status.receiver_status >> 7) & 0x01,
            (status.receiver_status >> 6) & 0x01,
            (status.receiver_status >> 5) & 0x01,
            (status.receiver_status >> 4) & 0x01,
            (status.receiver_status >> 3) & 0x01,
            antenna_mode, antenna_mode_str,
            (status.receiver_status >> 0) & 0x01,
            'INT' if ((status.receiver_status >> 0) & 0x01) else 'EXT',
            status.temperature/2.0)
            
        return msg
        
        
    def get_status(self):
        """
        Reads status informations from the <POSITION/STATUS/DATA MESSAGE>.
        Returns object containing all status infos in named tuple.
        """
        
        log.debug("Reading Status from <POSITION/STATUS/DATA> Message...")
        
        self.flush()    # Clear Input and Output Buffers
        self.msg(self.MSG_POSITION_STATUS_DATA_POLL)
        status_string = self.read(154)  # Answer is 154 Bytes
        
        if '_DEBUG_' in globals():
            log.debug('(debug mode, get_status returns bogus!)')
            status_string = ('\x00'*154).encode('ascii')
        
        status_string = bytes(status_string)
        status_struct_type = namedtuple('status', self.STRUCT_NAMES_STATUS)
        status_struct = status_struct_type._make(struct.unpack(self.STRUCT_FORMAT_STATUS, status_string))
        return status_struct
        
    
    def fix_current_position(self):
        """
        Saves current GPS coordinates into POSITION-HOLD POSITION Register.
        """
        #print(" Fixing Position...")
        #print(" Getting Status...")
        status = self.get_status()
        #print(" Current Position:\n   lat: %s\n   lon: %s\n   height: %s" % (status.lat_filt, status.long_filt, status.height_gps_filt))
        pos = [ (status.lat_filt>>24)    &0xff,
                (status.lat_filt>>16)    &0xff,
                (status.lat_filt>>8)     &0xff,
                (status.lat_filt>>0)     &0xff,
                (status.long_filt>>24)   &0xff,
                (status.long_filt>>16)   &0xff,
                (status.long_filt>>8)    &0xff,
                (status.long_filt>>0)    &0xff,
                (status.height_gps_filt>>24) &0xff,
                (status.height_gps_filt>>16) &0xff,
                (status.height_gps_filt>>8)  &0xff,
                (status.height_gps_filt>>0)  &0xff]
        pos_msg = ['A','s'] + pos + [0x00]
        self.msg(pos_msg)
        
        
    def get_position_hold_position(self):
        """
        Retrieves coordinates currently saved
        in POSITION-HOLD POSITION Register.
        """
        # Send POSITION-HOLD POSITION command with invalid coordinates, read answer.
        self.msg(self.MSG_POSITION_HOLD_POSITION_STATUS)
        status = self.read(20)
        
        # Read Lat/Lon/Height fields from the answer
        i = struct.unpack('>xxxxiiixxxx', status)
        return i
        
                               
    def enable_position_hold(self):
        """
        Activates Position Hold Mode with Coordinates
        currently saved in POSITION-HOLD POSITION Register.
        """
        self.msg(self.MSG_POSITION_HOLD_ENABLE)
        
        
    def disable_position_hold(self):
        """
        Disables Position Hold Mode
        """
        self.msg(self.MSG_POSITION_HOLD_DISABLE)
        
        
    def get_position_hold_status(self):
        """
        Gets current Position-Hold Status
        (0 = disabled,1 = position hold, 2 = altitude hold)
        """
        self.msg(self.MSG_POSITION_HOLD_STATUS)
        ret = self.read(8)
        return int(ret[4])
        
        
 
    #### LOW-LEVEL METHODS ###
    
    def __init__(self, serial_port='/dev/ttyAMA0'):
        """
        Creates new MotorolaGPS Instance and opens (specified)
        serial port connection.
        """
        self.serport = serial_port
        self.open_port()


    def __del__(self):
        """
        Destructor
        """
        self.close_port()


    def open_port(self):
        """
        Opens serial port if none has been opened yet.
        """
        log.debug('Opening Serial Port '+self.serport)
        
        if '_DEBUG_' in globals():
            log.debug('(debug mode, not opening)')
            return
            
        if self.uart is None:
            self.uart = serial.Serial(self.serport, timeout = 2)

        
    def close_port(self):
        """
        Tries to close serial port.
        """
        log.debug('Closing Serial Port.')
        
        if '_DEBUG_' in globals():
            log.debug('(debug mode, not closing)')
            return
            
        self.uart.close()

       
    def write(self, arrayofbytes):
        """
        Writes given bytearray to the serial port
        """
        log.debug("Write to UART: %s" % arrayofbytes)
        
        if '_DEBUG_' in globals():
            log.debug('(debug mode, not writing)')
            return
        
        self.uart.write(arrayofbytes)
        self.uart.flushOutput()         # Make sure Message is flushed from Output Buffer
        
        
    def read(self, size):
        """
        Reads given number of bytes from serial port
        """
        
        if '_DEBUG_' in globals():
            log.debug('(debug mode, not reading)')
            return 0
        
        return self.uart.read(size)
        

    def msg(self, message):
        """
        Writes given Motorola Binary Message to the Serial Port,
        adding Prefix, Checksum and Postfix
        """
        checksum = self.checksum(message)
        binaryMsg = self.to_bytes([0x40, 0x40] + message + [checksum] + 
                                  [0x0D, 0x0A])
        self.write(binaryMsg)
        
        
    def checksum(self, message):
        """
        Calculates checksum of given message (xor between each byte)
        """
        bytes = self.to_bytes(message)
        chksum = 0x00
        for b in bytes: 
            chksum = chksum ^ b
        return chksum
        
            
    def to_bytes(self,arr):
        """
        Converts a list of integers, characters or both into
        a list of bytes (ASCII codes for chars)
        """
        for (i,item) in enumerate(arr):
            if type(item) == str:
                arr[i] = ord(item)
        return bytearray(arr)
        
        
    def flush(self):
        """
        Flushes serial port input / output buffers.
        Discards all unread input messages.
        Immediately sends all unsent output messages.
        """
        
        if '_DEBUG_' in globals():
            log.debug('(debug mode, not flushing)')
            return
            
        self.uart.flushOutput()
        self.uart.flushInput()
        
        
    def reset(self):
        """
        Disables possibly activated continuous messages by setting
        ASCII-POSITION and POSITION/STATUS/DATA to polled mode.
        Should not be necessary...
        """
        self.flush()
        self.msg(self.MSG_ASCII_POSITION_POLL)
        self.msg(self.MSG_POSITION_STATUS_DATA_POLL)
        self.flush()



if __name__ == '__main__':
    
    print("Initializing GPS Connection...")
    gps = MotorolaGPS()
    
    print("Collecting Full status Report:")
    print(gps.info())
    
    exit()
    
    ## Do the Full test Suite:

    import time
    
    print("Reset GPS UART Settings...")
    gps.reset()
    time.sleep(1)
    gps.flush()
    print("... done.\n")

    print("Getting Status...")
    gps.print_status_report()

    print("Disabling Position Hold:")
    print(gps.disable_position_hold())

    print("Position Hold Status:")
    print(gps.get_position_hold_status())
    time.sleep(1)
    gps.flush()

    print("Fixing GPS Position:")
    print(gps.fix_current_position())
    time.sleep(1)
    gps.flush()

    print("Saved Position-Hold Coordinates:")
    print(gps.get_position_hold_position())
    time.sleep(1)
    gps.flush()

    print("Enabling Position Hold:")
    print(gps.enable_position_hold())
    time.sleep(1)
    gps.flush()

    print("Position Hold Status:")
    print(gps.get_position_hold_status())
    time.sleep(1)
    gps.flush()