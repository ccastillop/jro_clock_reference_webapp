#!/usr/bin/env python2.7

# Configure these Variables
fDDS = 360         # For list of possible Outputs
fDDS_low = 62.5    # Limits for fDDS
fDDS_high = 450    # Limits for fDDS
output = [60,80,10,0]  # For fDDS computations
suffix = 'MHz'

"""
This Module can be used for some calculations around the AD9548 fDDS Settings.
 - List all possible Output Frequencies for certain fDDS
 - Find lowest possible fDDS for desired Output Frequencies
 - Find an "ideal" fDDS with a maximum Output Freq. choice
"""


# Helper Functions

def gcd(a, b):
    """Return greatest common divisor using Euclid's Algorithm."""
    while b:      
        a, b = b, a % b
    return a

def lcm(a, b):
    """Return lowest common multiple."""
    return a * b // gcd(a, b)

def lcmm(args):
    """Return lcm of args."""   
    return reduce(lcm, args)
  

# List all possible Outputs for given Frequency
"""
print('Possible Output Frequencies for fDDS = %d%s' % (fDDS,suffix))
print('----------------------------------------------')

total = 0
for i in range(1, fDDS+1):
    if (1.0*fDDS/i == int(fDDS/i)):
        print('%d%s' % (i,suffix))
        total += 1
        
print('Total: %d Frequencies' % total)
print('------------------------')
"""


# Find lowest fDDS which allows required Output Frequencies

print('\nSearching fDDS for Output Freqs %s' % output)
print('----------------------------------------------------')

# Use Least Common Multiple of desired Frequencies
fDDS = lcmm(output)
# If resulting fDDS is below minimum Frequency, just scale up
if (fDDS<fDDS_low):
    fDDS = (int(fDDS_low/fDDS)+1)*fDDS
# If fDDS is too large, no solution exists
if (fDDS>fDDS_high):
    print('No such fDDS exists within the frequency limits.')
else:
    print('Use fDDS = %d%s' % (fDDS,suffix))


# Find Optimum Frequency in terms of largest choice of outputs
""""
tt = f = 0
for fDDS in range(fDDS_low, fDDS_high):
    numchildren = 0
    for i in range(1, fDDS+1):
        if (1.0*fDDS/i == int(fDDS/i)):
            numchildren += 1
    if numchildren > tt:
        tt = numchildren
        f = fDDS

print('\n\'Optimum\' fDDS: %d%s with %d possible Outputs' % (f,suffix,tt))
print('----------------------------------------------------')
"""
