% from random import random

% # Use Main Template with Title "Settings"
% rebase main title='Settings'

% # Optional Message

% if defined('message'):
<div id='message' class="ui-bar ui-bar-e">
        <div>
                {{!message}}
        </div>
</div>
&nbsp;
<script>
        setTimeout('$("#message").fadeOut()', 10000)
</script>
% end

% # Active Setting

<a data-role="button" data-theme="c" href="/settings?reload={{random()}}" style='overflow:scroll'>
        <div class="status-container" data-component='preset/active' >
                loading... <img src='/static/icons/load.gif'>
        </div>
</a>&nbsp;
<hr />

% # All possible Presets

% for num in range(0,4):
<div class="ui-grid-a"> 
        <div class="ui-block-a status-container" data-component='preset/{{num}}'>
                loading... <img src='/static/icons/load.gif'>
        </div>
        <div class="ui-block-b settingsbutton-container" data-component='preset/{{num}}'>
                <a href="/edit/preset/{{num}}" data-rel="dialog" data-mini="true" data-role="button" data-inline="true" data-icon="arrow-r">Edit</a>
                <a href="/activate/preset/{{num}}?reload={{random()}}" data-mini="true" data-role="button" data-inline="true" data-icon="arrow-r">Activate</a>
        </div>
</div>
<hr/>
% end
