% rebase main title='Status Overview'

% systemcomponents = [('ad9548', 'AD9548 Status'), ('gps', 'GPS Status'), ('syslog', 'System Log'), ('sysinfo', 'System Info')]

% for (component, title) in systemcomponents:

<div class="ui-grid-a"> 
        <div class="ui-block-a status-container" data-component='{{component}}'>
                loading... <img src='/static/icons/load.gif'>
        </div>
        <div class="ui-block-b infobutton-container" data-component='{{component}}'>
                <a href="/info/{{ component }}" data-rel="dialog" data-mini="true" data-role="button" data-inline="true" data-icon="arrow-r">{{ title }}</a>
        </div>
</div>
<hr>
% end
