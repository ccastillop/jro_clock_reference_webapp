#! /bin/bash

DIR="/home/debian/apps/jro_clock_reference_webapp"
DAEMON="nohup  /home/debian/.pyenv/versions/3.4.2/bin/python $DIR/webserver.py"
LOG="$DIR/log/jro_clock_reference_webapp.log"
ERR="$DIR/log/jro_clock_reference_webapp.log"
DAEMON_NAME="jro_clock_reference_webapp"

# Add any command line options for your daemon here
DAEMON_OPTS=""

# This next line determines what user the script runs as.
# Root generally not recommended but necessary if you are using the Raspberry Pi GPIO from Python.
DAEMON_USER=debian

# The process ID of the script when it runs is stored here:
PID_FILE="/tmp/$DAEMON_NAME.pid"

case $1 in
   start)
      $CMD >$LOG 2>$ERR  & echo $! >>$PID_FILE ;;
   stop)
      kill `cat $PID_FILE` ;;
   *)
      echo "usage: web {start|stop}" ;;
  esac
exit 0

# python ./webserver.py #&
