#!/bin/bash

USR="debian"
USR_DIR="/home/$USR"
WORK_DIR="$USR_DIR/apps/jro_clock_reference_webapp"
SCRIPT="$WORK_DIR/webserver.py"
LOG="$WORK_DIR/log/jro_clock_reference_webapp.log"
DAEMON_NAME="jro_clock_reference_webapp"
PID_FILE="/tmp/$DAEMON_NAME.pid"
PYENVDIR="$USR_DIR/.pyenv"
CMD="${PYENVDIR}/versions/3.4.2/bin/python $SCRIPT >$LOG 2>&1"

cd $WORK_DIR
$CMD & echo $! > $PID_FILE
#nohup $CMD & echo $! > $PID_FILE
echo "Started..... PID: `cat $PID_FILE`"
