from display import Display
from subprocess import call, Popen, PIPE
import time
import requests

#To get the string from list
def list2string(list_data):
    string_data = ''
    cont = 0
    for element in list_data:
        if cont<(len(list_data)-1):
            string_data = string_data + chr(element)
        cont = cont + 1

    return string_data


#IP
ip_data = Popen(["ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'"],shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
output0, err0 = ip_data.communicate(b"input data that is passed to subprocess' stdin")
ip_data = list(output0)
ip = list2string(ip_data)
ip_data = "IP: "+list2string(ip_data)

#Gateway
gw_data = Popen(["route -n | grep 'UG[ \t]' | awk '{print $2}'"],shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
output1, err1 = gw_data.communicate(b"input data that is passed to subprocess' stdin")
gwdata = list(output1)
gateway_data = "GW: "+list2string(gwdata)

#Mask
mask_data = Popen(["ifconfig eth0 | awk '/Mask:/{ print $4;} '"],shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
output2, err2 = mask_data.communicate(b"input data that is passed to subprocess' stdin")
mask_data = list(output2)
mask_data = list2string(mask_data)

#Port
port = '8000'
port_data = 'PORT: '+port

#try to active last preset
r1 = "http://"+ip+':'+port+'/activate/preset/0'
while(1):
    lcd = Display()
    lcd.println("Clock Generator", 1, 1)
    lcd.println("Synchronizer...",2,0)
    time.sleep(2)
    lcd.println("Loading last   ", 1, 1)
    lcd.println("configuration  ",2,0)
    time.sleep(2)
    lcd.println("please         ", 1, 1)
    lcd.println("wait for me :) ",2,0)
    time.sleep(2)
    try:
        frequencies = requests.get(r1, timeout = 0.5)
        lcd.println("Last config.", 1, 1)
        lcd.println("loaded... :D",2,0)
        time.sleep(2)
        break
    except:
        pass


#Frequencies
r = "http://"+ip+':'+port+'/frequencies/'

while(1):
    try:
        frequencies = requests.get(r, timeout = 0.5)
    except:
        frequencies = None

    if frequencies != None:
        frequencies = frequencies.json()
        frequencies = frequencies.get("Frecuencias")
        f0 = frequencies.get("f0")
        f1 = frequencies.get("f1")
        f2 = frequencies.get("f2")
        f3 = frequencies.get("f3")
    else:
        f0 = "---"
        f1 = "---"
        f2 = "---"
        f3 = "---"
#print(len(ip))
    f0 = "FREQ0: " + str(f0)
    f1 = "FREQ1: " + str(f1)
    f2 = "FREQ2: " + str(f2)
    f3 = "FREQ3: " + str(f3)


#Print LCD Screen
#while(1):
    lcd = Display()
    lcd.println(ip_data, 1, 1)
    lcd.println(f0,2,0)
    time.sleep(2)
    lcd.println(gateway_data,1,1)
    lcd.println(f1,2,0)
    time.sleep(2)
    lcd.println(mask_data,1,1)
    lcd.println(f2,2,0)
    time.sleep(2)
    lcd.println(port_data,1,1)
    lcd.println(f3,2,0)
    time.sleep(2)
#lcd.println(mask_data, 2, 0)
