from subprocess import call
#from components.logger import log


class Display:
    """
    UNTESTED CLASS!

    Provides Access to the I2C connected Display on the Front Panel.

    This is not yet tested and verified code because the Display is still in Transit...
    All work copied from Datasheet NHD-C0220BiZ-FSW-FBW-3V3M.pdf.
    """

    address = 0x3c

    def __init__(self):
        """
        Creates new Display Instance

        Tries to autodetect right I2C Bus from Raspberry Pi Board Revision Number.
        If it doesn't work, try hardcoding Bus Device in quick2wire_i2c.py
        """
        #log.info("Display instance started. Clearing Display.")
        self.init()
        # Tries to autodetect right I2C Bus from Version Number.
        # If it doesn't work, try hardcoding the Bus Number.


    def init(self):
        """
        Initializes Display.
        Sequence copied from Display Datasheet NHD-C0220BiZ-FSW-FBW-3V3M.pdf.
        """
        #log.info("Initializing Display.")
    
        clean_lcd = '0x00 0x38 0x39 0x14 0x78 0x5e 0x6d 0x0c 0x01 0x06 '
        self.i2c_send(clean_lcd)
  
    def println(self, text, row, clean):
        """
        Shows a ASCII string on LCD.
        Sequence copied from Display Datasheet NHD-C0220BiZ-FSW-FBW-3V3M.pdf.
        """
        #print("Print Text '%s' to Display." % text)
        
        #If clean_screen is necessary
        if clean == 1:
            self.clean_screen()
        #Choose the row of the LCD screen   
        if row == 2:
                row = '0x00 0xc0 '
                self.i2c_send(row)                
        else:
            row = '0x00 0x80 '
            self.i2c_send(row)

        #String to Hex
        txt = ''
        for t in text:
            c = hex(ord(t))
            txt = txt + c +' '
       
        try:
            self.i2c_send('0x40 '+txt)
        except:
            txt = '---'         

    def i2c_send(self, sequence):
        """
        Sends the given list of Bytes over I2C Bus.
        """

        #print('i2c_send(%s)' % (sequence))
        text = 'i2cset -y 1 '+str(self.address)+' '+sequence+'i'
        call([str(text)],shell=True)

    def clean_screen(self):
        """
        Clear LCD screen
        """
        clean_lcd = '0x00 0x38 0x39 0x14 0x78 0x5e 0x6d 0x0c 0x01 0x06 '
        self.i2c_send(clean_lcd)

#lcd_display = Display()
#lcd_display.println('Nothing to do here')
