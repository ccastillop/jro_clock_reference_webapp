#Introduction

This web app permits setup and admin the AD6584 Clock Reference card, 
in order to generate stable and precise clock references for radar use.
This app must be installed on the Raspberry Pi

## PRE-setup:

use pyenv
http://lovebug356.blogspot.pe/2016/01/build-your-own-python-version-on.html

install python 3.4 `pyenv install 3.4.2`

## Setup

install bottle 
`pip install bottle`

Install i2c modules and tools
`sudo apt-get install python-smbus`

REf: https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c

User pi permisions

`sudo usermod -G i2c pi`

