#! /usr/bin/env python


from bottle import route, run, template, static_file, view, request

from info_mux import Infos
from components.logger import log


#########
## Define all possible URL Routes
#########

# STATIC FILES
@route('/static/<file:path>')
def returnStaticFile(file):
    return static_file(file, root='./static')

@route('/favicon')


# MAIN STATUS PAGE
@route('/')
@route('/status')
@view('status')
def status_template():
    return dict()
    

# MAIN SETTINGS PAGE
@route('/settings')
@view('settings')
def settings_template():
    return dict()
    

# EXTENDED INFORMATIONS DISPLAYED IN DIALOG for Components
@route('/info/<component>')
@view('dialog')
def get_component_info(component):
    if component=='ad9548':
        return dict(title='AD9548 Status', content='<pre>%s</pre>'%infos.ad9548.info())
    if component=='gps':
        return dict(title='GPS Status', content='<pre>%s</pre>'%infos.gps.info())
    if component=='syslog':
        return dict(title='System Log', content='<pre>%s</pre>'%infos.system.syslog())
    if component=='sysinfo':
        return dict(title='System Info', content='<pre>%s</pre>'%infos.system.info())
    else:
        return '<pre>Could not find Component %s!</pre>' % component


# STATUS ICON + STATUS MESSAGE for Components
@route('/status/<component>')
@view('icon_short_status')
def get_component_status(component):
    # Components on Status Page
    if component == 'ad9548':
        icon, status = infos.ad9548.status()
        return dict(icon=icon, text='AD9548: '+status)
    if component == 'gps':
        icon, status = infos.gps.status()
        return dict(icon=icon, text='GPS: '+status)
    if component == 'syslog':
        icon = 'okay'
        status = 'running'
        return dict(icon=icon, text='Raspberry Pi: '+status)
    if component == 'sysinfo':
        icon = 'time'
        status = infos.system.uptime()
        return dict(icon=icon, text='Uptime: '+status)
    else:
        return 'Could not find Component %s!' % component
        
        
# PRESET STATUS for Settings
@route('/status/preset/<num>')
@view('icon_short_status')
def get_component_status(num):
    
    # When asked for 'active' preset, read directly from AD9548
    if (num == 'active'):
        # Return currently set System Frequency
        icon = 'frequency'
        freq = infos.ad9548.get_frequencies()
        status = infos.format_frequencies(freq)
        return dict(icon=icon, text='Active: %s' % (status))
        
    # When asked for preset 'num', read from presets File
    else:
        num = int(num)
        # Check Validity of Preset Number
        if not (0 <= int(num) <= 3):
            return 'Invalid Preset %s!' % num
        # Update Presets from File
        infos.presets.load()
        p = infos.presets.presets[num]
        # Icon indicates 'active' property of Preset
        icon = ('active' if p['active'] else 'inactive')
        # Status prints current Frequency Config, with OFF for Frequency 0
        status = infos.format_frequencies(p['f'])
        # Spit it out, Watson
        return dict(icon=icon, text='Preset %s: %s' % (num, status))
           

# SETUP DIALOG FOR FREQUENCY PRESETS
@route('/edit/preset/<num:int>')
@view('dialog_freqedit')
def get_settings_dialog(num):
    # Check Validity of Preset Number
    if not (0 <= int(num) <= 3):
        return dict(title='Not found', content='Invalid Preset %s!' % num)
    # Update Presets from File
    infos.presets.load()
    p = infos.presets.presets[num]
    # Icon indicates 'active' property of Preset
    icon = ('active' if p['active'] else 'inactive')
    # Status prints current Frequency Config, with OFF for Frequency 0
    status = infos.format_frequencies(p['f'])
    # Spit it out, Watson
    return dict(preset=num, freqs=p['f'])


# SAVE UPDATED FREQUENCY PRESET
@route('/save/preset/<num:int>/<f0:int>/<f1:int>/<f2:int>/<f3:int>/')
# Text-Only Output
def save_preset(num, f0, f1, f2, f3):
    
    # Check Validity of Preset Number
    if not (0 <= int(num) <= 3):
        log.warning('Trying to Save to invalid Preset %s' % num)
        return ('Invalid Preset %s!' % num)
    # Check Validity of Frequencies
    f = [f0, f1, f2, f3]
    for freq in f:
        if not (0 <= freq <= 450e6):
            log.warning('Trying to Save to invalid Frequency %s' % freq)
            return ('Invalid Frequency %s ' % freq)
    
    # Update Presets Manager
    infos.presets.load()
    # Save to Presets Manager
    infos.presets.update_freq(num, f)
    # Save Presets to File
    infos.presets.save()
    
    log.info('Saved new Frequencies %s to Preset %s' % (f, num))
    
    # If this was the active Profile, send the new Frequency to AD9548!
    if infos.presets.presets[num]['active']:
        success = infos.ad9548.set_frequencies(f0,f1,f2,f3)
        
        if (success == -1):
            log.warning('Sent new Frequency set %s to AD9548,\
                         but update Failed.' % (num))    
            title = "Updating Frequency Failed."
            status = "<p>The Frequencies could not be set.</p>"
            status += "<p>Note that not all Frequency combinations are "
            status += "possible, because all outputs are post-divided from a "
            status += "common intermediate frequency fDDS which is limited "
            status += "between 62.5MHz and 450MHz.</p>"
            infos.presets.deactivate(num)
            infos.presets.save()
        elif (success == 0):
            log.info('Sent new Frequency set %s to AD9548. Update succesful.'
                     % (num))    
            title = "Frequency Update Successful"
            status = "The Output Frequencies were successfully adjusted."
            
        return '<h3>%s</h3><p>%s</p>' % (title, status)
    
    # This was not the active Frequency, so we just saved the Preset.
    return 'Frequency Preset saved.'
        

# ACTIVATE A DIFFERENT PROFILE
@route('/activate/preset/<num:int>')
@view('settings')
def activate_profile(num):
    
    # Check Validity of Preset Number
    if not (0 <= int(num) <= 3):
        log.warning('Trying to Activate invalid Preset %s' % num)
        return ('Invalid Preset %s!' % num)
        
    # Update Presets, then save new Active state
    log.info('Requested Activation of Preset 2')
    infos.presets.load()
    infos.presets.activate(num)
    infos.presets.save()
    log.info('Activated Preset %s' % (num))
    
    # Now send Frequencies from Activated Preset to AD9548
    f = infos.presets.presets[num]['f']
    log.info('Sending Frequencies %s to AD9548' % (f))
    success = infos.ad9548.set_frequencies(f[0],f[1],f[2],f[3])
    
    if (success == -1):
        log.warning('Sent new Frequency set %s to AD9548, but update Failed.'
                    % (num))    
        message = "<h4>Updating Frequency Failed.</h4>"
        message += "<p>The Frequencies could not be set.</p>"
        message += "<p>Note that not all Frequency combinations are possible, "
        message += "because all outputs are post-divided from a common "
        message += "intermediate frequency fDDS which is limited "
        message += "between 62.5MHz and 450MHz.</p>"
        infos.presets.deactivate(num)
        infos.presets.save()
    elif (success == 0):
        log.info('Sent new Frequency set %s to AD9548. Update succesful.'
                 % (num))    
        message = "Frequency Update Successful"
    
    return dict(message = message)
    


######################################################
#####################################################
# SEND FREQUENCIES
#@route('/frequencies/<f0:int>/<f1:int>/<f2:int>/<f3:int>/', method='POST')
@route('/frequencies/', method='POST')
# Text-Only Output
#Module "request" has been added: from bottle import request

#def save_preset(f0, f1, f2, f3):
def set_freq():
    num=0
    
    f0 = int(request.forms.get("f0"))
    f1 = int(request.forms.get("f1"))
    f2 = int(request.forms.get("f2"))
    f3 = int(request.forms.get("f3"))

    # Check Validity of Frequencies
    f = [f0, f1, f2, f3]
    for freq in f:
        if freq == 0:
            continue
        if not (1 <= freq <= 450e6):
            log.warning('Trying to Save to invalid Frequency %s' % freq)
            return ('Invalid Frequency %s ' % freq)

    # Update Presets Manager
    infos.presets.load()
    # Save to Presets Manager
    infos.presets.update_freq(num, f)
    # Save Presets to File
    infos.presets.save()

    log.info('Saved new Frequencies %s' % (f))

 # Send the new Frequency to AD9548!
    
    success = infos.ad9548.set_frequencies(f0,f1,f2,f3)

    if (success == -1):
        log.warning('Sent new Frequencies to AD9548, but update Failed.')
        title = "Updating Frequency Failed."
        status = "The Frequencies could not be set."
        status += "Note that not all Frequency combinations are "
        status += "possible, because all outputs are post-divided from a "
        status += "common intermediate frequency fDDS which is limited "
        status += "between 62.5MHz and 450MHz."
        infos.presets.deactivate(num)
        infos.presets.save()

    elif (success == 0):
        

        ############################	
        # Update Presets, then save new Active state
        log.info('Requested Activation of Preset 2')
        infos.presets.load()
        infos.presets.activate(num)
        infos.presets.save()
        #log.info('Activated Preset %s' % (num))

        # Now send Frequencies from Activated Preset to AD9548
        #f = infos.presets.presets[num]['f']
        log.info('Sending Frequencies %s to AD9548' % (f))
        #success = infos.ad9548.set_frequencies(f[0],f[1],f[2],f[3])

        message = "Frequency Update Successful"
        #message = {"f0":f0, "f1":f1, "f2":f2, "f3":f3}

       #return dict(Frecuencias = message)
	##############################


        log.info('Sent new Frequencies to AD9548. Update succesful.')
        title = "Frequency Update Successful"
        title = "okay"
        status = "The Output Frequencies were successfully adjusted."

	
    return '%s,%s' % (title, status)    
    #return { "success" : False, "error" : tittle + status}
    #return 'Settting Frequencies Done.'
#####################################################################
########################################################################
    

#Get Frequencies
@route('/frequencies/', method='GET')
def getfrequencies():
    freqs = infos.ad9548.get_frequencies()
    message = {"f0":freqs[0], "f1":freqs[1], "f2":freqs[2], "f3":freqs[3]}
    return dict(Frecuencias = message)

    
########
# Start the Webserver
########

# Infos Object is responsible for Collecting / Distributing all Informations
infos = Infos()

run(host='0.0.0.0', port=8000, debug=True)
log.info('New WebApp Instance Started')
